<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id="";

    public $booktitle="";

    public $author_name="";


    public function __construct(){



        parent::__construct();

    }

    public function setData($data=NULL)
    {


        if (array_key_exists('id', $data)) {


            $this->id = $data['id'];
        }

        if (array_key_exists('booktitle', $data)) {


            $this->booktitle = $data['booktitle'];
        }

        if (array_key_exists('author_name', $data)) {


            $this->author_name= $data['author_name'];
        }
    }
    public function store(){










        $sql="insert into book_title(booktitle,author_name)values
('$this->booktitle','$this->author_name')";
        $STH=$this->conn->prepare($sql);

        $STH->execute();




    }
    public function index($Mode="ASSOC"){
        $Mode = strtoupper($Mode);
        $STH = $this->conn->query('SELECT * from book_title');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

//


}// end of BookTitle